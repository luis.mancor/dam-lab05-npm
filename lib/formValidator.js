//Debe validar obligatorios y opcionales
//validar fechas correctas e emails

exports.formValidate = function(usuario, pass, email, fecha){

    var resultadoFunction = '';
    var emailVal = /\S+@\S+\.\S+/; //valida email
    var dateVal = /^\d{2}([./-])\d{2}\1\d{4}$/; //Valida fecha
    
    var emailValidado = emailVal.test(email);
    var dateValidado = dateVal.test(fecha);
    
    if(usuario == '' || pass == ''){
        resultadoFunction = "Necesita llenar campos obligatorios."
        return resultadoFunction;
    }else{
        if(email == '' && fecha == ''){
            resultadoFunction = "Campos no olbligatorios llenados correctamente!"
            return resultadoFunction;
        }else{
            if(emailValidado === false){
                resultadoFunction = "Necesita ingresar un email valido."
                return resultadoFunction;
            }else if(dateValidado === false){
                resultadoFunction = "Necesita ingresar una fecha valida. (DD/MM/YYYY)"
                return resultadoFunction;
            }else{
                resultadoFunction = "Datos validados correctamente!"
                return resultadoFunction;
            }
        }
    }
};


