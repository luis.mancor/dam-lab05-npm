exports.calcularNavidad = function(){
    var today = new Date();
    var cmas = new Date(today.getFullYear(),11,25);  //Equivale al 25 dic
    
    if (today.getMonth()==11 && today.getDate()> 25){
        cmas.setFullYear(cmas.getFullYear()+1)
    }
    const un_dia = 1000*60*60*24;
    var daysLeftTillNextXmass = Math.ceil((cmas.getTime()-today.getTime())/(un_dia));
    
    return daysLeftTillNextXmass;
}